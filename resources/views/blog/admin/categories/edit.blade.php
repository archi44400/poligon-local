@extends('layouts.app')

@section('content')
    @php
        /** @var \App\Models\BlogCategory $item */
    @endphp
    <form
        method="POST"
        action="{{ route('blog.admin.categories.update', $item->id) }}"
    >
        @method('PATCH')
        @csrf
        <div class="container">
            @php
            /** @var \Illuminate\Support\ViewErrorBag $errors */
            /** @var \LaravelIdea\Helper\Illuminate\Notifications\ $success */
            @endphp
            @if($errors->any() || session('success') || session('warning'))
                <div class="row justify-content-center">
                    <div class="col-md-11">
                        <div
                            class="alert @if($errors->any()) alert-danger @elseif(session('success')) alert-success @else alert-warning @endif"
                            role="alert"
                        >
                            <button type="submit" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            @if($errors->any())
                                {{ $errors->first() }}
                            @elseif(session('success'))
                                {{ session('success') }}
                            @else {{ session('warning') }} @endif
                        </div>
                    </div>
                </div>
            @endif
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('blog.admin.categories.includes.item_edit_main_col')
                </div>
                <div class="col-md-3">
                    @include('blog.admin.categories.includes.item_edit_add_col')
                </div>
            </div>
        </div>
    </form>
@endsection
